package com.example.exercice1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private SensorManager sensorManager;
    LinearLayout parentLayout;
    LayoutInflater layoutInflater;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sensorManager =(SensorManager)getSystemService(Context.SENSOR_SERVICE);
        setContentView(R.layout.activity_main);
        parentLayout = (LinearLayout)findViewById(R.id.layout);
        layoutInflater = getLayoutInflater();
        showSensorList();
    }

    // afficher la liste des capteurs disponibles sur le smartphone
    private void showSensorList(){
        List <Sensor> sensorList =sensorManager.getSensorList(Sensor.TYPE_ALL);
        StringBuffer sensorDesc ;
        for (Sensor sensor:sensorList){
            sensorDesc =new StringBuffer();
            sensorDesc.append("\t"+getString(R.string.name)+" "+sensor.getName()+"\r\n");
            sensorDesc.append("\t"+getString(R.string.type)+" "+sensor.getType()+"\r\n");
            sensorDesc.append("\t"+getString(R.string.resolution)+" "+sensor.getResolution()+"\r\n");
            sensorDesc.append("\t"+getString(R.string.power)+" "+sensor.getPower()+"\r\n");
            sensorDesc.append("\t"+getString(R.string.maxRange)+sensor.getMaximumRange()+"\r\n");
            sensorDesc.append("\t"+getString(R.string.minDelay)+sensor.getMinDelay()+"\r\n");
            view = layoutInflater.inflate(R.layout.text_layout, parentLayout, false);
            TextView textView = (TextView)view.findViewById(R.id.text);
            textView .setText(sensorDesc.toString());
            textView.setTextColor(Color.BLACK);
            parentLayout.addView(textView);
        }
    }
}