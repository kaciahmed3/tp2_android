package com.example.exercice4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor accelerometre;
    private TextView tv;
    private float [] previousDirections;
    private String horisantalDirection;
    private String verticalDirection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)!=null){
            accelerometre=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this,accelerometre,SensorManager.SENSOR_DELAY_NORMAL);
            tv=(TextView)findViewById(R.id.textViewDirection);
            previousDirections=new float[2];

        }else{
            showErrorAccelerometerAbsent();
        }

    }
    // informer l'utilisisateur si l'accéléromètre est indisponible dans l'appareil.
    public void showErrorAccelerometerAbsent(){
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(getString(R.string.titleDialog));
        alert.setMessage(getString(R.string.accAbsent));
        alert.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.btnOK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.show();
    }

    @Override
    protected void onPause() {
        sensorManager.unregisterListener(this,accelerometre);
        super.onPause();
    }

    @Override
    protected void onResume() {
        sensorManager.registerListener(this,accelerometre,SensorManager.SENSOR_DELAY_NORMAL);
        super.onResume();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            float xChange;
            float yChange;

            xChange=previousDirections[0]-event.values[0];
            yChange=previousDirections[1]-event.values[1];

            previousDirections[0]= event.values[0];
            previousDirections[1]=event.values[1];
            //
            if (xChange < -2){
                horisantalDirection=getString(R.string.left);
            }else if (xChange > 2){
                horisantalDirection=getString(R.string.right);
            }
            if (yChange > 2){
                verticalDirection=getString(R.string.top);
            }else if (yChange < -2){
                verticalDirection=getString(R.string.bottom);
            }
            tv.setText(getString(R.string.direction )+" x : "+horisantalDirection+"   y: "+verticalDirection);
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}