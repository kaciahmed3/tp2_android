package com.example.exercice2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private SensorManager sensorManager;
    Button btnShowAgain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager =(SensorManager)getSystemService(Context.SENSOR_SERVICE);
        showInformationMessage();
        btnShowAgain=(Button)findViewById(R.id.button);
        btnShowAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              showInformationMessage();
            }
        });
    }
    /* afficher une boite de dialogue pour informer l'utilisateur de l'indisponibilité de certaines
        fonctionnalités suite à la détection de l'indisponibilité d'un capteur donné sur son appareil*/
    private void showInformationMessage() {
       StringBuffer msgSensor = new StringBuffer();
       /* j'ai choisi ce capteur  car il est indisponible*/
            if (sensorManager.getDefaultSensor(Sensor.TYPE_LOW_LATENCY_OFFBODY_DETECT) == null)
            {
                msgSensor.append("\t" + getString(R.string.msgSensorAbsent)+"\r\n");
                AlertDialog alert = new AlertDialog.Builder(this).create();
                alert.setTitle(getString(R.string.titleDialog));
                alert.setMessage(msgSensor);
                alert.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.btnOK), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                   // ne rien faire
                    }
                });
                alert.show();
            }else{
                msgSensor.append("\t" + getString(R.string.msgSensorPresent)+"\r\n");
                Toast.makeText(this,msgSensor,Toast.LENGTH_LONG).show();
            }
    }
}