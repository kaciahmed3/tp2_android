package com.example.exercice3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    Sensor accelerometer;
    ConstraintLayout principalLayout;

    private float x = 0;
    private float y = 0;
    private float z = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager=(SensorManager)getSystemService(Context.SENSOR_SERVICE);
        principalLayout=(ConstraintLayout)findViewById(R.id.principalLayout);
        // vérifier la présence de l'accéléromètre.
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)!=null){
            accelerometer=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }else{
            Toast.makeText(this,getString(R.string.accAbsent),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,accelerometer,sensorManager.SENSOR_DELAY_UI);

    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this,accelerometer);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // vérifier que le type de l'événement capté est bien celui de l'accéléromètre
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            // récupérer les valeurs du capteur
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            // calculer l'acccélération en fonction des valeurs de l'accéléromètre
            double accelerationVal = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)) - SensorManager.GRAVITY_EARTH;

            //les valeurs sont inférieurs
            if (accelerationVal < 18) {
                principalLayout.setBackgroundColor(Color.GREEN);
            } else {
                //les valeurs sont moyenne
                if (accelerationVal <= 35) {
                    principalLayout.setBackgroundColor(Color.BLACK);
                } else {
                        //les valeurs sont suppérieur
                        if (accelerationVal > 35) {
                            principalLayout.setBackgroundColor(Color.RED);
                        }
                  }
              }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}