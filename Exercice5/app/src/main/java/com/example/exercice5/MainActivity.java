package com.example.exercice5;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;

import java.util.EventListener;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor accelerometre;
    private CameraDevice cameraDevice;
    private CameraManager cameraManager;
    private CameraCharacteristics cameraCharacteristics;
    private static final float THRESHOLD_SHAKE=15.0f;
    private static int MIN_DELAY_SHAKES=1000; // en milliseconde
    private long lastShakeTime;
    private String idCamera;
    private Boolean flashON;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager=(SensorManager)getSystemService((SENSOR_SERVICE));
        accelerometre=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometre!= null){
            sensorManager.registerListener((SensorEventListener) this,accelerometre,SensorManager.SENSOR_DELAY_NORMAL);
        }else{
            showErrorAccelerometerAbsent();
        }
        boolean isFlashAvailable = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
        if(isFlashAvailable) {
            cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                idCamera = cameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }else{
            showErrorFlashAbsent();
        }

        lastShakeTime=0;
        flashON=false;
    }

    // informer l'utilisisateur si l'accéléromètre est indisponible dans l'appareil.
    public void showErrorAccelerometerAbsent(){
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(getString(R.string.titleDialog));
        alert.setMessage(getString(R.string.accAbsent));
        alert.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.btnOK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.show();
    }

    // informer l'utilisateur de l'indisponibilité du flash
    public void showErrorFlashAbsent(){
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(getString(R.string.titleDialog2));
        alert.setMessage(getString(R.string.msgDialog2));
        alert.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.btnOK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener((SensorEventListener) this,accelerometre,SensorManager.SENSOR_DELAY_NORMAL);
        }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener((SensorEventListener) this,accelerometre);
        try {
            cameraManager.setTorchMode(idCamera,false);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            long currentTime=System.currentTimeMillis();
            if (currentTime-lastShakeTime>MIN_DELAY_SHAKES){
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                // calcule de l'accélération
                double accelerationVal =Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)) - SensorManager.GRAVITY_EARTH;
                if (accelerationVal > THRESHOLD_SHAKE){
                    lastShakeTime=currentTime;
                    if (flashON){
                        try {
                            cameraManager.setTorchMode(idCamera,false);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                        flashON=false;
                    }else{
                        try {
                            cameraManager.setTorchMode(idCamera,true);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                        flashON=true;

                    }
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}