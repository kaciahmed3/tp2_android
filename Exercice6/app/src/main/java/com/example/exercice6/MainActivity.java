package com.example.exercice6;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    float proximityValue;
    SensorManager sensorManager;
    Sensor proximitySensor;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        proximitySensor= sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (proximitySensor== null){
            showErrorProximitySensorAbsent();
        }
        img = (ImageView)findViewById(R.id.imageView1);
    }
    // informer l'utilisisateur si le capteur de proximité  est indisponible dans l'appareil.
    public void showErrorProximitySensorAbsent(){
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(getString(R.string.titleDialog));
        alert.setMessage(getString(R.string.proximityUnavalable));
        alert.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.btnOK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,proximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this,proximitySensor);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_PROXIMITY){
            if (event.sensor.getType()==Sensor.TYPE_PROXIMITY){
                proximityValue=event.values[0];
                if (proximityValue==0){
                    img.setImageDrawable(getDrawable(R.drawable.proche));
                    Toast.makeText(getApplicationContext(),getString(R.string.near),Toast.LENGTH_SHORT).show();
                }else{
                    img.setImageDrawable(getDrawable(R.drawable.loin));
                    Toast.makeText(getApplicationContext(),getString(R.string.far),Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}